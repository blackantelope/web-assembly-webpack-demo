import '../styles/index.scss';

const loader = require("assemblyscript/lib/loader/index");

// fetch("public/build/optimized.wasm")
// .then(response =>
//     response.arrayBuffer()
// ).then(bytes =>{
//     const lib = loader.instantiateBuffer(bytes, {});
//     console.log(lib.getString(lib.something()))
//     console.log(lib.getString(lib.toItem(lib.newString("hi"))))
// }).catch(err => {
//   alert("Failed to load WASM: " + err.message + " (ad blocker, maybe?)");
//   console.log(err.stack);
// });

document.querySelector("#form").addEventListener("submit", function(e){
    e.preventDefault();
   // console.log(document.getElementById('addstring').value)
    let str = document.getElementById('addstring').value;
    let contents = document.getElementById('contents');
    fetch("public/build/optimized.wasm")
    .then(response =>
        response.arrayBuffer()
    ).then(bytes =>{
        const lib = loader.instantiateBuffer(bytes, {});   
        let item = lib.getString(lib.toItem(lib.newString(str)));
        var textnode = document.createTextNode(item);
        contents.appendChild(textnode);  
    }).catch(err => {
      alert("Failed to load WASM: " + err.message + " (ad blocker, maybe?)");
      console.log(err.stack);
    });
});