import "allocator/tlsf";

export { memory };

// The entry file of your WebAssembly module.
export function toItem(str: string): string {
  return '<item>'+str+'</item>';
}

export function something(): string {
  return '<trans-unit>soimething</trans-unit>';
}