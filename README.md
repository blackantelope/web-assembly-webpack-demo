# Web Assembly Webpack Demo

Steps to build
======

###### install modules

```
npm install
```

###### compile webassembly binaries form ts

```
npm run asbuild
```

Dev server
======

###### run dev server

```
npm run dev
```


Build Static Site
======


```
npm run build
```




Gotcha's
======

See the postinstall which fetches the latest master of assemblyscript.  We're working off the latest here as we want some of the loader features that deal with easier memory access that dealing with pointers.

Webpack loading WASM files throws and error currently so when running the dev server, use Firefox for now.